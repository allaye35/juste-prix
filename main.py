from datetime import datetime, time, date
import time
from time import strftime
import random
import constants
import csv

def fonctionInput():
    nombreSasierParUtilusateur = input('veuillez sasir la valeur entre 0 et 200')
    debut = heure()
    print(debut)
    if nombreSasierParUtilusateur!="":
        nombreSasierParUtilusateurInt = int(nombreSasierParUtilusateur)
        return nombreSasierParUtilusateurInt

def saisiUtilusateur():
    while True:
        nombreSasierParUtilusateurInts=fonctionInput()
        print('nombreSasierParUtilusateur',nombreSasierParUtilusateurInts)
        if  nombreSasierParUtilusateurInts>= valeurMin and nombreSasierParUtilusateurInts<=valeurMax:
            print('vous avez saisir la bonne valeur',nombreSasierParUtilusateurInts)
            return nombreSasierParUtilusateurInts
        else:
            print("erreur de saisier : choissisez un entier rentre 0 et 200",nombreSasierParUtilusateurInts)


def fonctionAleatoire():
    nombreAleatoire=random.randint(valeurMin,valeurMax)
    print("nombreAleatoire",nombreAleatoire)
    return nombreAleatoire



def ValeurDeComparaison():
    tempsDebut = time.time()
    resultatNombreAleatoire=fonctionAleatoire()
    print("resultatNombreAleatoire", resultatNombreAleatoire)
    nombreDessaie=1
    score=0
    while True:
        resultatnombreSasierParUtilusateur=saisiUtilusateur()
        print("resultatnombreSasierParUtilusateur",resultatnombreSasierParUtilusateur)
        tempsActuel = time.time()
        tempsPasse = tempsActuel - tempsDebut
        tempsEcoule = tempsEstIlEcoule(tempsPasse)
        if tempsEcoule:
            print("Desolé, le temps de jeu est écoulé !")
        elif resultatNombreAleatoire==resultatnombreSasierParUtilusateur:
            score += 1
            print("vous avez gagne avec un score ",score)
            return print("score",score)

        elif resultatNombreAleatoire>resultatnombreSasierParUtilusateur:
            print("vous avez saisie une valeur plus petite")
            saisiUtilusateur()
        elif resultatNombreAleatoire<resultatnombreSasierParUtilusateur:
            print("vous avez saisie une valeur plus grande")
            saisiUtilusateur()
        nombreDessaie += 1
        print("nombreDessaie",nombreDessaie)
        return nombreDessaie

def date():
    dateActuelle = time.strftime('%Y-%m-%d', time.localtime())
    print("dateActuelle", dateActuelle)
    return dateActuelle
def heure():
    HeureActuelle=time.time()
    print("HeureActuelle",HeureActuelle)
    return HeureActuelle

def tempsEstIlEcoule(tempsPasse):
    if tempsPasse <= constants.TEMPSJEU:
        return False
    return True


def scoreDejeu(tempsPasse):
    "nombreDessaie=0"
    score= 0
    resultatnombreSasierParUtilusateur=saisiUtilusateur()
    resultatNombreAleatoire = fonctionAleatoire()
    tempsDejeu=tempsEstIlEcoule(tempsPasse)
    while tempsDejeu==True and resultatnombreSasierParUtilusateur==resultatNombreAleatoire:
        score +=1
    return score
def resultatDuJeuFormatTexte():
    fichier = open("texte.csv", "wt")
    ecrivainCSV = csv.writer(fichier, delimiter=";")
    ecrivainCSV.writerow(["Nom", "Prénom", "Téléphone"])  # On écrit la ligne d'en-tête avec le titre des colonnes
    ecrivainCSV.writerow(["Dubois", "Marie", "0198546372"])
    ecrivainCSV.writerow(["Duval", "Julien \"Paul\"", "0399741052"])
    ecrivainCSV.writerow(["Jacquet", "Bernard", "0200749685"])
    ecrivainCSV.writerow(["Martin", "Julie;Clara", "0399731590"])
    fichier.close()

def resultatDuJeuFormatJson():
    import json
    quantiteFournitures = {"cahiers": 134, "stylos": {"rouge": 41, "bleu": 74}, "gommes": 85}
    fichier = open("quantiteFournitures.json", "wt")
    fichier.write(json.dumps(quantiteFournitures))
    fichier.close()


valeurMin=0
valeurMax=200
if __name__ == '__main__':
    s=ValeurDeComparaison()
    print(s)